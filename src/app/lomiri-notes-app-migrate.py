#!/usr/bin/python3

import os
import sys
from pathlib import Path

organization = "notes.ubports"
application = "notes.ubports"
old_application = "com.ubuntu.reminders"

xdg_config_home = Path(os.environ.get("XDG_CONFIG_HOME",
                                      Path.home() / ".config"))
for old_name, new_name in (
    (f"{old_application}.conf", f"{application}.conf"),
    ("reminders.conf", "notes.conf")
):
    old_config_file = xdg_config_home / organization / old_name
    new_config_file = xdg_config_home / organization / new_name
    if old_config_file.is_file() and not new_config_file.exists():
        old_config_file.rename(new_config_file)

if len(sys.argv) > 1:
    os.execvp(sys.argv[1], sys.argv[1:])
