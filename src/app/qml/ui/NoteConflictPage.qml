/*
 * Copyright: 2015 Canonical, Ltd
 *
 * This file is part of reminders
 *
 * reminders is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * reminders is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.4
import Lomiri.Components 1.3
import Lomiri.Components.Popups 1.3
import Evernote 0.1

import "../components"

Page {
    id: root

    property var note

    header: PageHeader {
        id: header
        title: i18n.tr("Conflicting changes")
    }

    Column {
        anchors.top: header.bottom
        anchors.bottom: parent.bottom
        anchors.left: parent.left
        anchors.right: parent.right

        NotesAppListItem {
            summaryText: i18n.tr("This note has been modified in multiple places. Open each version to check the content, then swipe to keep one of them.")
            iconName: "weather-severe-alert-symbolic"
            progressionVisible: false
        }

        NotesAppListItem {
            titleText: root.note.deleted ? i18n.tr("Deleted locally:") : i18n.tr("Modified locally:")
            iconVisible: false
            progressionVisible: false
        }

        NotesAppListItem {
            titleText: root.note.title
            subtitleText: Qt.formatDateTime(root.note.updated, Qt.LocalDate)
            summaryText: root.note.tagline
            iconVisible: false
            progressionVisible: false
            clip: true
            action: Action {
                onTriggered: {
                    apl.addPageToCurrentColumn(root, Qt.resolvedUrl("NotePage.qml"), { note: root.note, readOnlyMode: true })
                }
            }
            trailingActions: ListItemActions {
                actions: [
                    Action {
                        text: i18n.tr("Keep")
                        iconName: "ok"

                        onTriggered: {
                            var popup = PopupUtils.open(Qt.resolvedUrl("../components/ResolveConflictConfirmationDialog.qml"), root,
                                                        { keepLocal: true, localDeleted: root.note.deleted, remoteDeleted: root.note.conflictingNote.deleted })

                            popup.accepted.connect(function() {
                                NotesStore.resolveConflict(root.note.guid, NotesStore.KeepLocal)
                                apl.removePages(root)
                            })
                        }
                    }
                ]
            }
        }

        NotesAppListItem {
            titleText: root.note.conflictingNote.deleted ? i18n.tr("Deleted on Evernote:") : i18n.tr("Modified on Evernote:")
            iconVisible: false
            progressionVisible: false
        }

        NotesAppListItem {
            titleText: root.note.conflictingNote.title
            subtitleText: Qt.formatDateTime(root.note.conflictingNote.updated, Qt.LocalDate)
            summaryText: root.note.conflictingNote.tagline
            iconVisible: false
            progressionVisible: false
            clip: true
            action: Action {
                onTriggered: {
                    apl.addPageToCurrentColumn(root, Qt.resolvedUrl("NotePage.qml"), { note: root.note.conflictingNote, readOnlyMode: true })
                }
            }
            trailingActions: ListItemActions {
                actions: [
                    Action {
                        text: i18n.tr("Keep")
                        iconName: "ok"

                        onTriggered: {
                            var popup = PopupUtils.open(Qt.resolvedUrl("../components/ResolveConflictConfirmationDialog.qml"), root,
                                                        { keepLocal: false, localDeleted: root.note.deleted, remoteDeleted: root.note.conflictingNote.deleted })

                            popup.accepted.connect(function() {
                                NotesStore.resolveConflict(root.note.guid, NotesStore.KeepRemote)
                                apl.removePages(root)
                            })
                        }
                    }
                ]
            }
        }
    }
}
