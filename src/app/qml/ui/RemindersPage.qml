/*
 * Copyright: 2013 - 2014 Canonical, Ltd
 *
 * This file is part of reminders
 *
 * reminders is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * reminders is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.4
import QtQuick.Layouts 1.1
import Lomiri.Components 1.3
import Lomiri.Components.ListItems 1.3
import Lomiri.Components.Popups 1.3
import Evernote 0.1

import "../components"

Item {
    id: root
    objectName: "remindersPage"
    anchors.fill: parent

    property var page

    Notes {
        id: reminders
        onlyReminders: true
    }

    LomiriListView {
        id: remindersListView
        anchors.fill: parent
        pullToRefresh.enabled: false // reminders are not synced with Evernote
        currentIndex: -1 // Workaround: LomiriListView seems to auto-highlight the 1st item
        clip: true
        model: reminders
        delegate: ReminderDelegate {
            width: parent.width
            height: units.gu(8)
            note: model

            onOpenReminder: {
                displayNote(NotesStore.note(model.guid));
            }
            onDeleteReminder: {
                var popup = PopupUtils.open(Qt.resolvedUrl("../components/DeleteConfirmationDialog.qml"),
                                            root,
                                            { targetType: i18n.tr("reminder"), targetTitle: Qt.formatDateTime(note.reminderTime, Qt.LocalDate) })
                popup.confirmed.connect(function() {
                    reminders.note(model.guid).reminder = false
                    reminders.note(model.guid).hasReminderTime = false
                    NotesStore.saveNote(model.guid)
                })
            }
            onEditReminder: {
                print("catched")
                apl.addPageToNextColumn(root.page, Qt.resolvedUrl("SetReminderPage.qml"), { note: model })
            }
        }
    }

    Label {
        anchors.centerIn: parent
        visible: !reminders.loading && remindersListView.count == 0
        width: parent.width - units.gu(4)
        wrapMode: Text.WordWrap
        horizontalAlignment: Text.AlignHCenter
        text: i18n.tr("No reminders available")
    }
}
