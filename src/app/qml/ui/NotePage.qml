/*
 * Copyright: 2013 Canonical, Ltd
 *
 * This file is part of reminders
 *
 * reminders is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * reminders is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.4
import Lomiri.Components 1.3
import Lomiri.Components.Popups 1.3
import Lomiri.Components.Themes.Ambiance 1.3
import QtWebEngine 1.7
import Lomiri.Content 1.3
import Evernote 0.1

Page {
    id: root

    property var note
    property bool readOnlyMode: false

    header: PageHeader {
        id: header

        property string notebookColor: {
            if (settings.notebookColorsEnabled)
                return preferences.colorForNotebook(note.notebookGuid)
            else
                return theme.palette.normal.foregroundText
        }

        title: root.note.title
        style: PageHeaderStyle {
            foregroundColor: header.notebookColor
        }
        trailingActionBar.actions: [
            Action {
                text: i18n.tr("Edit")
                iconName: "edit"
                visible: !root.readOnlyMode

                onTriggered: {
                    apl.addPageToCurrentColumn(root, Qt.resolvedUrl("EditNotePage.qml"), { note: root.note })
                }
            },
            Action {
                text: i18n.tr("Filters")
                iconName: "filters"
                visible: !root.readOnlyMode

                onTriggered: {
                    apl.addPageToCurrentColumn(root, Qt.resolvedUrl("EditNoteFiltersPage.qml"), { note: root.note })
                }
            }
        ]
        extension: Rectangle {
            width: header.width * ( noteView.loadProgress / 100 )
            height: units.gu(0.5)
            color: header.notebookColor
        }
    }

    WebEngineView {
        id: noteView
        anchors.top: header.bottom
        anchors.bottom: parent.bottom
        anchors.left: parent.left
        anchors.right: parent.right

        property string html

        zoomFactor: 2.5

        Component.onCompleted: {
            noteView.html = root.note.htmlContent
            noteView.url = "file://" + NoteHelper.notePath

            NoteHelper.writeNote(noteView.html, theme.palette.normal.background, theme.palette.normal.foregroundText)
            noteView.reload()
        }

        Connections {
            target: NotesStore

            onNoteChanged: {
                noteView.html = root.note.htmlContent

                NoteHelper.writeNote(noteView.html, theme.palette.normal.background, theme.palette.normal.foregroundText)
                noteView.reload()
            }
        }

        Connections {
            target: wsServer

            onClientConnected: {
                print("Websocket client connected")

                webSocket.onTextMessageReceived.connect(function(message) {
                    print("Websocket server recieved: " + message)

                    var data = JSON.parse(message)

                    switch (data.type) {
                        case "checkboxChanged":
                            root.note.markTodo(data.todoId, data.checked)
                            break
                        case "attachmentOpened":
                            var filePath = root.note.resource(data.resourceHash).hashedFilePath

                            if (data.mediaType == "application/pdf") {
                                contentPeerPicker.contentType = ContentType.Documents
                            }
                            else if (data.mediaType.split("/")[0] == "audio" ) {
                                contentPeerPicker.contentType = ContentType.Music
                            }
                            else if (data.mediaType.split("/")[0] == "image" ) {
                                contentPeerPicker.contentType = ContentType.Pictures;
                            }
                            else if (data.mediaType == "application/octet-stream" ) {
                                contentPeerPicker.contentType = ContentType.All
                            }
                            else {
                                contentPeerPicker.contentType = ContentType.Unknown
                            }

                            contentPeerPicker.filePath = filePath
                            contentPeerPicker.visible = true
                            break
                    }
                })
            }
        }
    }

    ContentItem {
        id: exportItem
        name: i18n.tr("Attachment")
    }

    ContentPeerPicker {
        id: contentPeerPicker
        visible: false
        contentType: ContentType.Unknown
        handler: ContentHandler.Destination
        anchors.fill: parent

        property string filePath: ""

        onPeerSelected: {
            var transfer = peer.request()

            if (transfer.state === ContentTransfer.InProgress) {
                var items = new Array()
                var path = contentPeerPicker.filePath
                exportItem.url = path
                items.push(exportItem)
                transfer.items = items
                transfer.state = ContentTransfer.Charged
            }

            contentPeerPicker.visible = false
        }
        onCancelPressed: {
            contentPeerPicker.visible = false
        }
    }

    Component.onDestruction: {
        if (!root.readOnlyMode) {
            NotesStore.saveNote(root.note.guid)
        }
    }
}
