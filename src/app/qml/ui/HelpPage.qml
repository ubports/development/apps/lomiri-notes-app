/*
 * Copyright: 2021 UBports
 *
 * This file is part of reminders
 *
 * reminders is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * reminders is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.4
import Lomiri.Components 1.3

import "../components"

Page {
    id: root
    objectName: "helpPage"

    property int selectedIndex: -1

    header: PageHeader {
        title: i18n.tr("Help")
    }

    Column {
        anchors.top: parent.header.bottom
        anchors.bottom: parent.bottom
        anchors.left: parent.left
        anchors.right: parent.right
        spacing: units.gu(1)

        NotesAppListItem {
            titleText: i18n.tr("Report an issue")
            subtitleText: "Gitlab"
            iconName: "external-link"
            action: Action {
                onTriggered: {
                    Qt.openUrlExternally("https://gitlab.com/ubports/development/apps/lomiri-notes-app/-/issues")
                }
            }
        }

        NotesAppListItem {
            titleText: i18n.tr("Introducing the new navigation panel")
            subtitleText: i18n.tr("Go ahead and select an icon below")
            iconName: "history"
            progressionVisible: false
        }

        Row {
            id: navigationPanel
            anchors.horizontalCenter: parent.horizontalCenter
            width: (noteActionIcon.width * 4) + (spacing * 3)
            height: units.gu(5)
            spacing: units.gu(1)

            ActionIcon {
                id: noteActionIcon
                iconName: "note"
                index: 0
                selectedIndex: root.selectedIndex

                onSelected: {
                    root.selectedIndex = noteActionIcon.index
                    explanation.text = i18n.tr("Notes")
                }
            }

            ActionIcon {
                id: notebookActionIcon
                iconName: "notebook"
                index: 1
                selectedIndex: root.selectedIndex

                onSelected: {
                    root.selectedIndex = notebookActionIcon.index
                    explanation.text = i18n.tr("Notebooks")
                }
            }

            ActionIcon {
                id: tagActionIcon
                iconName: "tag"
                index: 2
                selectedIndex: root.selectedIndex

                onSelected: {
                    root.selectedIndex = tagActionIcon.index
                    explanation.text = i18n.tr("Tags")
                }
            }

            ActionIcon {
                id: reminderActionIcon
                iconName: "reminder"
                index: 3
                selectedIndex: root.selectedIndex

                onSelected: {
                    root.selectedIndex = reminderActionIcon.index
                    explanation.text = i18n.tr("Reminders")
                }
            }
        }

        Label {
            id: explanation
            anchors.horizontalCenter: parent.horizontalCenter
            width: navigationPanel.width
            height: units.gu(5)
            textSize: Label.Medium
            horizontalAlignment: Text.AlignHCenter
        }
    }
}
