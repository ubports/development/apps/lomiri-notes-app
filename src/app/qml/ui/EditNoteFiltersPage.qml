/*
 * Copyright: 2021 UBports
 *
 * This file is part of reminders
 *
 * reminders is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * reminders is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.4
import Lomiri.Components 1.3
import Lomiri.Components.Themes.Ambiance 1.3
import Lomiri.Components.Popups 1.3
import Evernote 0.1

import "../components"

Page {
    id: root

    property var note

    header: PageHeader {
        id: header

        property string notebookColor: {
            if (settings.notebookColorsEnabled)
                return preferences.colorForNotebook(root.note.notebookGuid)
            else
                return theme.palette.normal.foregroundText
        }

        title: root.note.title
        style: PageHeaderStyle {
            foregroundColor: header.notebookColor
        }
        extension: Rectangle {
            width: header.width
            height: units.gu(0.5)
            color: header.notebookColor
        }
    }

    Column {
        anchors.top: header.bottom
        anchors.left: root.left
        anchors.right: root.right

        NotesAppListItem {
            titleText: i18n.tr("Notebook")
            subtitleText: NotesStore.notebook(root.note.notebookGuid).name
            iconName: "notebook"
            action: Action {
                onTriggered: {
                    var popup = PopupUtils.open(Qt.resolvedUrl("../components/SelectNotebookDialog.qml"), root,
                                                { notebookGuid: root.note.notebookGuid })

                    popup.selected.connect(function(newNotebookGuid) {
                        root.note.notebookGuid = newNotebookGuid
                        NotesStore.saveNote(root.note.guid)
                    })
                }
            }
        }

        NotesAppListItem {
            titleText: i18n.tr("Tags")
            subtitleText: (root.note.tagNames.length > 0) ? root.note.tagNames.join(", ") : " "
            iconName: "tag"
            action: Action {
                onTriggered: {
                    var popup = PopupUtils.open(Qt.resolvedUrl("../components/SelectTagsDialog.qml"), root,
                                                { tagGuids: root.note.tagGuids })

                    popup.selected.connect(function(newTagGuids) {
                        root.note.tagGuids = newTagGuids
                        NotesStore.saveNote(root.note.guid)
                    })
                }
            }
        }

        NotesAppListItem {
            titleText: i18n.tr("Reminder")
            subtitleText: root.note.reminder ? Qt.formatDateTime(NotesStore.note(root.note.guid).reminderTime, Qt.LocalDate) : " "
            iconName: "reminder"
            action: Action {
                onTriggered: {
                    apl.addPageToCurrentColumn(root, Qt.resolvedUrl("SetReminderPage.qml"), { note: root.note })
                }
            }
        }

        NotesAppListItem {
            titleText: i18n.tr("Details")
            subtitleText: " "
            iconName: "info"
            action: Action {
                onTriggered: {
                    var popup = PopupUtils.open(Qt.resolvedUrl("../components/NoteDetailsDialog.qml"), root,
                                                { note: root.note })
                }
            }
        }
    }
}
