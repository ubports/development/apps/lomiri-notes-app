/*
 * Copyright: 2014 Canonical, Ltd
 *
 * This file is part of reminders
 *
 * reminders is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * reminders is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.4
import Lomiri.Components 1.3
import Lomiri.Components.Popups 1.3
import Lomiri.Components.ListItems 1.3
import Evernote 0.1

Dialog {
    id: root
    title: i18n.tr("Select tags")

    property var tagGuids

    signal selected(var newTagGuids)

    Tags {
        id: tags
    }

    text: tags.count == 0 ? i18n.tr("No tags available") : ""

    OptionSelector {
        id: tagsSelector

        property var selectedTagGuids: new Set()

        expanded: true
        multiSelection: true
        model: tags
        delegate: OptionSelectorDelegate {
            text: model.name

            onClicked: {
                if(selected) {
                    tagsSelector.selectedTagGuids.add(model.guid)
                }
                else {
                    tagsSelector.selectedTagGuids.delete(model.guid)
                }
            }

            Component.onCompleted: {
                selected = false

                for(var i = 0; i < root.tagGuids.length; ++i) {
                    if(model.guid == root.tagGuids[i]) {
                        selected = true
                        tagsSelector.selectedTagGuids.add(model.guid)
                        break
                    }
                }
            }
        }
    }

    Button {
        text: i18n.tr("Select")
        color: theme.palette.normal.positive
        enabled: tags.count != 0

        onClicked: {
            root.selected(Array.from(tagsSelector.selectedTagGuids))
            PopupUtils.close(root)
        }
    }

    Button {
        text: i18n.tr("Cancel")

        onClicked: {
            PopupUtils.close(root)
        }
    }
}
