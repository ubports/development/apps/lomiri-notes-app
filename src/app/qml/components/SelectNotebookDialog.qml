/*
 * Copyright: 2021 UBports
 *
 * This file is part of reminders
 *
 * reminders is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * reminders is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.4
import Lomiri.Components 1.3
import Lomiri.Components.Popups 1.3
import Lomiri.Components.ListItems 1.3
import Evernote 0.1

Dialog {
    id: root
    title: i18n.tr("Select notebook")

    property var notebookGuid

    signal selected(string newNotebookGuid)

    Notebooks {
        id: notebooks
    }

    OptionSelector {
        id: notebookSelector
        expanded: true
        model: notebooks
        delegate: OptionSelectorDelegate {
            text: model.name
        }

        Component.onCompleted: {
            for (var i = 0; i < notebooks.count; ++i) {
                if (notebooks.notebook(i).guid == root.notebookGuid) {
                    notebookSelector.selectedIndex = i
                    break
                }
            }
        }
    }

    Button {
        text: i18n.tr("Select")
        color: theme.palette.normal.positive

        onClicked: {
            root.selected(notebooks.notebook(notebookSelector.selectedIndex).guid)
            PopupUtils.close(root)
        }
    }

    Button {
        text: i18n.tr("Cancel")

        onClicked: {
            PopupUtils.close(root)
        }
    }
}
