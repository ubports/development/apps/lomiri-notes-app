/*
 * Copyright: 2021 UBports
 *
 * This file is part of reminders
 *
 * reminders is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * reminders is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.4
import Lomiri.Components 1.3

ListItem {
    id: root
    height: _content.height + (divider.visible ? divider.height : 0)
    divider.colorFrom: theme.palette.normal.foreground
    divider.colorTo: theme.palette.normal.foreground

    property string titleText
    property string subtitleText
    property string summaryText
    property alias iconName: _icon.name
    property alias iconVisible: _icon.visible
    property alias progressionVisible: _progression.visible

    ListItemLayout {
        id: _content
        title.text: root.titleText
        subtitle.text: root.subtitleText
        summary.text: root.summaryText
        summary.textSize: Label.Medium
        summary.color: theme.palette.normal.foregroundText
        summary.wrapMode: Text.WordWrap

        Icon {
            id: _icon
            SlotsLayout.overrideVerticalPositioning: true
            SlotsLayout.position: SlotsLayout.Leading
            anchors.verticalCenter: parent.verticalCenter
            width: units.gu(3)
            height: width
        }

        ProgressionSlot {
            id: _progression
            SlotsLayout.overrideVerticalPositioning: true
            SlotsLayout.position: SlotsLayout.Trailing
            anchors.verticalCenter: parent.verticalCenter
        }
    }
}
