/*
 * Copyright: 2020 UBports
 *
 * This file is part of reminders
 *
 * reminders is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * reminders is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.4
import Lomiri.Components 1.3
import Evernote 0.1

ListItem {
    id: root
    height: units.gu(12)
    // Workaround: LomiriListView doesn't add dividers to dynamically added ListItems
    // We are implementing our own divider
    divider.visible: false

    property var note
    property string notebookColor: {
        if (settings.notebookColorsEnabled)
            return preferences.colorForNotebook(note.notebookGuid)
        else
            return theme.palette.normal.foregroundText
    }
    property string tags: {
        var tags = new Array()
        for(var i = 0; i < note.tagGuids.length; i++) {
            tags.push(NotesStore.tag(note.tagGuids[i]).name)
        }
        return tags.join(" ")
    }

    signal openNote()
    signal deleteNote()
    signal editNote()
    signal openFilters()

    leadingActions: ListItemActions {
        actions: [
            Action {
                iconName: "delete"
                enabled: !root.note.conflicting

                onTriggered: root.deleteNote()
            }
        ]
    }
    action: Action {
        onTriggered: root.openNote()
    }
    trailingActions: ListItemActions {
        actions: [
            Action {
                text: i18n.tr("Filters")
                iconName: "filters"
                enabled: !root.note.conflicting

                onTriggered: {
                    root.openFilters()
                }
            },
            Action {
                text: i18n.tr("Edit")
                iconName: "edit"
                enabled: !root.note.conflicting

                onTriggered: root.editNote()
            }
        ]
    }

    Column {
        anchors.fill: parent

        Item {
            width: parent.width
            height: parent.height - noteDivider.height

            Row {
                anchors.fill: parent
                anchors.margins: units.gu(1)
                spacing: units.gu(1)

                Column {
                    width: parent.width - parent.spacing - noteStats.width
                    height: parent.height

                    Label {
                        id: titleLabel
                        width: parent.width
                        text: root.note.title
                        textSize: Label.Medium
                        color: root.notebookColor
                        font.strikeout: root.note.deleted
                    }

                    Label {
                        id: dateLabel
                        width: parent.width
                        text: Qt.formatDateTime(root.note.updated, Qt.LocalDate)
                        textSize: Label.Small
                    }

                    Label {
                        width: parent.width
                        height: parent.height - titleLabel.height - dateLabel.height - tagsLabel.height
                        text: root.note.tagline
                        textSize: Label.Small
                        maximumLineCount: 2
                        wrapMode: Text.WrapAtWordBoundaryOrAnywhere
                        font.strikeout: root.note.deleted
                        textFormat: Text.PlainText
                    }

                    Label {
                        id: tagsLabel
                        width: parent.width
                        text: root.tags
                        textSize: Label.XSmall
                        maximumLineCount: 1
                        wrapMode: Text.WordWrap
                    }
                }

                Item {
                    id: noteStats
                    width: units.gu(2)
                    height: parent.height

                    Icon {
                        visible: root.note.reminder
                        height: width
                        anchors.top: parent.top
                        anchors.right: parent.right
                        anchors.left: parent.left
                        name: "alarm-clock"
                    }
                    Icon {
                        visible: preferences.accountName != "@local"
                        height: width
                        anchors.right: parent.right
                        anchors.bottom: parent.bottom
                        anchors.left: parent.left
                        name: root.note.loading ? "sync-updating" :
                              root.note.syncError ? "sync-error" :
                              root.note.synced ? "sync-idle" :
                              root.note.conflicting ? "weather-severe-alert-symbolic" : "sync-offline"
                    }
                    Icon {
                        visible: preferences.accountName == "@local"
                        height: width
                        anchors.right: parent.right
                        anchors.bottom: parent.bottom
                        anchors.left: parent.left
                        source: "../images/sync-none.svg"
                    }
                }
            }
        }

        Rectangle {
            id: noteDivider
            width: parent.width
            height: units.gu(0.1)
            color: theme.palette.normal.base
        }
    }
}
