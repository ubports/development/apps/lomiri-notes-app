/*
 * Copyright: 2013 Canonical, Ltd
 *
 * This file is part of reminders
 *
 * reminders is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * reminders is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.4
import QtQuick.Layouts 1.1
import QtQuick.Controls.Suru 2.2
import Lomiri.Components 1.3
import Lomiri.Components.ListItems 1.3
import Evernote 0.1

ListItem {
    id: root
    // Workaround: LomiriListView doesn't add dividers to dynamically added ListItems
    // We are implementing our own divider
    divider.visible: false

    property var notebook
    property string notebookColor: {
        if (settings.notebookColorsEnabled)
            return preferences.colorForNotebook(notebook.guid)
        else
            return theme.palette.normal.foregroundText
    }

    signal openNotebook()
    signal deleteNotebook()
    signal renameNotebook()
    signal setAsDefault()

    leadingActions: ListItemActions {
        actions: [
            Action {
                text: i18n.tr("Delete")
                iconName: "delete"
                enabled: !root.notebook.isDefaultNotebook

                onTriggered: root.deleteNotebook()
            }
        ]
    }
    action: Action {
        onTriggered: root.openNotebook()
    }
    trailingActions: ListItemActions {
        actions: [
            Action {
                text: i18n.tr("Set as default")
                iconName: root.notebook.isDefaultNotebook ? "starred" : "non-starred"
                enabled: !root.notebook.isDefaultNotebook

                onTriggered: root.setAsDefault()
            },
            Action {
                text: i18n.tr("Rename")
                iconName: "edit"

                onTriggered: root.renameNotebook()
            }
        ]
    }

    Column {
        anchors.fill: parent

        Item {
            width: parent.width
            height: parent.height - notebookDivider.height

            Row {
                anchors.fill: parent
                anchors.margins: units.gu(1)

                Label {
                    width: parent.width - notebookStats.width
                    height: parent.height
                    text: root.notebook.name
                    color: root.notebookColor
                    verticalAlignment: Text.AlignVCenter
                    font.strikeout: root.notebook.deleted
                }

                Item {
                    id: notebookStats
                    width: units.gu(2)
                    height: parent.height

                    Column {
                        anchors.fill: parent

                        Label {
                            width: parent.width
                            height: width
                            text: "(" + root.notebook.noteCount + ")"
                            textSize: Label.XSmall
                            horizontalAlignment: Text.AlignHCenter
                        }
                        Icon {
                            width: units.gu(2)
                            height: width
                            source: "../images/sync-none.svg"
                            visible: preferences.accountName == "@local"
                        }
                        Icon {
                            width: parent.width
                            height: width
                            name: root.notebook.loading ? "sync-updating" :
                                  root.notebook.syncError ? "sync-error" :
                                  root.notebook.synced ? "sync-idle" : "sync-offline"
                            visible: preferences.accountName != "@local"
                        }
                    }
                }
            }
        }

        Rectangle {
            id: notebookDivider
            width: parent.width
            height: units.gu(0.1)
            color: theme.palette.normal.base
        }
    }
}
