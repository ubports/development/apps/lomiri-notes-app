﻿/*
 * Copyright: 2013 - 2014 Canonical, Ltd
 *
 * This file is part of reminders
 *
 * reminders is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * reminders is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.4
import QtQuick.Layouts 1.1
import QtQuick.Controls.Suru 2.2
import Lomiri.Components 1.3
import Lomiri.Components.Popups 1.3
import Lomiri.Components.ListItems 1.3
import Lomiri.Connectivity 1.0
import Evernote 0.1
import Lomiri.OnlineAccounts 0.1
import Lomiri.OnlineAccounts.Client 0.1
import Lomiri.PushNotifications 0.1
import Lomiri.Content 1.0
import Qt.labs.settings 1.0
import QtWebSockets 1.0

import "components"
import "ui"

MainView {
    id: root

    objectName: "mainView"
    applicationName: "notes.ubports"

    property bool narrowMode: root.width < units.gu(140)

    property var uri: undefined

    signal themeChanged()

    onNarrowModeChanged: {
        if (narrowMode) {
            // Clean the toolbar
            //notesPage.selectedNote = null;
        }
    }

    Settings {
        id: settings
        category: "reminderSettings"
        property string appTheme: "System"
        property bool welcomePageEnabled: true
        property bool notebookColorsEnabled: true
    }

    AdaptivePageLayout {
        id: apl
        anchors.fill: parent
    }

    Connections {
        target: NotesStore

        onLoadedChanged: {
            print("NS loaded: " + NotesStore.loaded)
            if(NotesStore.loaded) {
                apl.primaryPageSource = Qt.resolvedUrl("ui/MainPage.qml")
            }
        }
    }

    Connections {
        target: UriHandler
        onOpened: {
            root.uri = uris[0];
            processUri();
        }
    }

    Connections {
        target: NetworkingStatus
        onStatusChanged: {
            switch (NetworkingStatus.status) {
            case NetworkingStatus.Offline:
                EvernoteConnection.disconnectFromEvernote();
                break;
            case NetworkingStatus.Online:
                // Seems DNS still fails most of the time when we get this signal.
                connectDelayTimer.start();
                break;
            }
        }
    }

    property var importTransfer: null
    function handleImportTransfer(note) {
        if (importTransfer == null) return;

        for (var i = 0; i < importTransfer.items.length; i++) {
            var url = importTransfer.items[i].url;
            switch (importTransfer.contentType) {
            case ContentType.Links:
                note.insertLink(note.plaintextContent.length, url)
                break;
            default:
                note.attachFile(note.plaintextContent.length, url)
                break;
            }
        }
        note.save();
        importTransfer = null;
    }

    Connections {
        target: ContentHub
        onImportRequested: {
            importTransfer = transfer;
            var popup = PopupUtils.open(importQuestionComponent);
            popup.accepted.connect(function(createNew) {
                PopupUtils.close(popup);
                if (createNew) {
                    var note = NotesStore.createNote(i18n.tr("Untitled"));
                    handleImportTransfer(note);
                }
            })

            popup.rejected.connect(function() {
                PopupUtils.close(popup);
                importTransfer = null;
            })
        }
    }

    Timer {
        id: connectDelayTimer
        interval: 2000
        onTriggered: EvernoteConnection.connectToEvernote();
    }

    Connections {
        target: EvernoteConnection
        onIsConnectedChanged: {
            if (EvernoteConnection.isConnected && root.uri) {
                processUri();
            }
        }
    }

    function displayNote(note, page) {
        if (importTransfer != null) {
            handleImportTransfer(note);
        }

        if (page == null) {
            page = apl.primaryPage
        }

        print("displayNote:", note.guid)
        note.load(true);

        if (note.conflicting) {
            apl.addPageToNextColumn(page, Qt.resolvedUrl("ui/NoteConflictPage.qml"), { note: note })
        }
        else {
            apl.addPageToNextColumn(page, Qt.resolvedUrl("ui/NotePage.qml"), { note: note })
        }
    }

    function switchToEditMode(note) {
        note.load(true)

        apl.addPageToNextColumn(apl.primaryPage, Qt.resolvedUrl("ui/EditNotePage.qml"), { note: note })
    }

    function doLogin() {
        var accountName = preferences.accountName;
        if (accountName == "@local") {
            as.startAuthentication("@local", null);
            return;
        }

        if (accountName) {
            print("Last used account:", accountName);
            var i;
            for (i = 0; i < accounts.count; i++) {
                if (accounts.get(i, "displayName") == accountName) {
                    print("Account", accountName, "still valid in Online Accounts.");
                    as.startAuthentication(accounts.get(i, "displayName"), accounts.get(i, "accountServiceHandle"));
                    return;
                }
            }
        }

        switch (accounts.count) {
        case 0:
            PopupUtils.open(noAccountDialog);
            print("No account available. Please set up an account in System Settings.");
            break;
        case 1:
            print("Connecting to account", accounts.get(0, "displayName"), "as there is only one account available");
            as.startAuthentication(accounts.get(0, "displayName"), accounts.get(0, "accountServiceHandle"));
            break;
        default:
            print("There are multiple accounts. Allowing user to select one.");
            apl.addPageToCurrentColumn(root, Qt.resolvedUrl("ui/AccountSelectorPage.qml"), { accounts: accounts })
        }
    }

    function processUri() {
        // var commands = root.uri.split("://")[1].split("/");
        // if (EvernoteConnection.isConnected && commands && NotesStore) {
        //     switch(commands[0].toLowerCase()) {
        //         case "notes": // evernote://notes
        //             rootTabs.selectedTabIndex = 0;
        //             break;

        //         case "note": // evernote://note/<noteguid>
        //             if (commands[1]) {
        //                 var note = NotesStore.note(commands[1])
        //                 if (note) {
        //                     displayNote(note);
        //                 } else {
        //                     console.warn("No such note:", commands[1])
        //                 }
        //             }
        //             break;

        //         case "newnote": // evernote://newnote  or  evernote://newnote/<notebookguid>
        //             if (commands[1]) {
        //                 if (NotesStore.notebook(commands[1])) {
        //                     NotesStore.createNote(i18n.tr("Untitled"), commands[1]);
        //                 } else {
        //                     console.warn("No such notebook.");
        //                 }
        //             } else {
        //                 NotesStore.createNote(i18n.tr("Untitled"));
        //             }
        //             break;

        //         case "editnote": // evernote://editnote/<noteguid>
        //             if (commands[1]) {
        //                 var note = NotesStore.note(commands[1]);
        //                 displayNote(note);
        //                 switchToEditMode(note);
        //             }
        //             break;

        //         case "notebooks": // evernote://notebooks
        //             rootTabs.selectedTabIndex = 1;
        //             break;

        //         case "notebook": // evernote://notebook/<notebookguid>
        //             if (commands[1]) {
        //                 if (NotesStore.notebook(commands[1])) {
        //                     notebooksPage.openNotebook(commands[1]);
        //                 } else {
        //                     console.warn("No such notebook:", commands[1]);
        //                 }
        //             }
        //             break;

        //         case "reminders": // evernote://reminders
        //             rootTabs.selectedTabIndex = 2;
        //             break;

        //         case "tags": // evernote://tags
        //             rootTabs.selectedTabIndex = 3;
        //             break;

        //         case "tag": // evernote://tag/<tagguid>
        //             if (commands[1]) {
        //                 // tagsPage.openTaggedNotes(commands[1]);
        //             }
        //             break;

        //         default: console.warn('WARNING: Unmanaged URI: ' + commands);
        //     }
        //     commands = undefined;
        // }
    }

    function registerPushClient() {
        console.log("Registering push client", JSON.stringify({
                                                                  "userId" : "" +  UserStore.userId,
                                                                  "appId": root.applicationName + "_notes",
                                                                  "token": pushClient.token
                                                              }));
        var req = new XMLHttpRequest();
        req.open("post", "https://push.ubports.com/gateway/register", true);
        req.setRequestHeader("content-type", "application/json");
        req.onreadystatechange = function() {//Call a function when the state changes.
            print("push client register response")
            if(req.readyState == 4) {
                if (req.status == 200) {
                    print("PushClient registered")
                } else {
                    print("Error registering PushClient:", req.status, req.responseText, req.statusText);
                }
            }
        }
        req.send(JSON.stringify({
            "userId" : "" + UserStore.userId,
            "appId": root.applicationName + "_notes",
            "token": pushClient.token
        }))
    }

    PushClient {
        id: pushClient
        appId: root.applicationName + "_notes"

        onNotificationsChanged: {
            print("Received PushClient notifications:", notifications.length)
            for (var i = 0; i < notifications.length; i++) {
                print("notification", i, ":", notifications[i])
                var notification = JSON.parse(notifications[i])["payload"];

                if (notification["userId"] != UserStore.userId) { // Yes, we want type coercion here.
                    console.warn("user mismatch:", notification["userId"], "!=", UserStore.userId)
                    return;
                }

                switch(notification["reason"]) {
                case "update":
                    print("Note updated on server:", notification["guid"])
                    if (NotesStore.note(notification["guid"]) === null) {
                        NotesStore.refreshNotes();
                    } else {
                        NotesStore.refreshNoteContent(notification["guid"]);
                    }
                    break;
                case "create":
                    print("New note appeared on server:", notification["guid"])
                    NotesStore.refreshNotes();
                    break;
                case "notebook_update":
                    NotesStore.refreshNotebooks();
                    break;
                default:
                    console.warn("Unhandled push notification:", notification["reason"])
                }
            }
        }

        onError: {
            console.warn("PushClient Error:", error)
        }
    }

    AccountServiceModel {
        id: accounts
        applicationId: "notes.ubports_notes"
        service: useSandbox ? "notes.ubports_notes-sandbox" : "notes.ubports_notes"
    }

    AccountService {
        id: as
        function startAuthentication(username, objectHandle) {
            //Load the cache
            EvernoteConnection.disconnectFromEvernote();
            EvernoteConnection.token = "";
            NotesStore.username = username;
            preferences.accountName = username;
            if (username === "@local" && !preferences.haveLocalUser) {
                NotesStore.createNotebook(i18n.tr("Default notebook"));
                preferences.haveLocalUser = true;
            }

            if (objectHandle === null) {
                return;
            }

            as.objectHandle = objectHandle;
            // FIXME: workaround for lp:1351041. We'd normally set the hostname
            // under onAuthenticated, but it seems that now returns empty parameters
            EvernoteConnection.hostname = as.authData.parameters["HostName"];
            authenticate(null);
        }

        onAuthenticated: {
            EvernoteConnection.token = reply.AccessToken;
            print("token is:", EvernoteConnection.token)
            print("NetworkingStatus.online:", NetworkingStatus.Online)
            if (NetworkingStatus.Online) {
                EvernoteConnection.connectToEvernote();
            }
        }
        onAuthenticationError: {
            console.log("Authentication failed, code " + error.code)
        }
    }

    Component.onCompleted: {
        i18n.domain = "lomiri-notes-app";

        if (tablet) {
            width = units.gu(100);
            height = units.gu(75);
        } else if (phone) {
            width = units.gu(40);
            height = units.gu(75);
        }

        doLogin();

        if (uriArgs) {
            root.uri = uriArgs[0];
        }
        setCurrentTheme();
    }

    /*
      Sets the system theme according to the theme selected
      under settings.
    */
    function setCurrentTheme() {
        if (settings.appTheme == "System") {
          theme.name = "";
        } else if (settings.appTheme == "SuruDark") {
          theme.name = "Lomiri.Components.Themes.SuruDark"
        } else if (settings.appTheme == "Ambiance") {
          theme.name = "Lomiri.Components.Themes.Ambiance"
        } else {
          theme.name = "";
        }
    }

    onThemeChanged: setCurrentTheme()

    Connections {
        target: UserStore
        onUserChanged: {
            print("Logged in as user:", UserStore.userId, UserStore.userName);
            preferences.setTokenForUser(UserStore.userId, EvernoteConnection.token);
            if (UserStore.userId >= 0) {
                registerPushClient();
            }
        }
    }

    // Column {
    //     id: statusBar
    //     anchors { left: parent.left; right: parent.right; top: parent.top; topMargin: units.gu(6)}

    //     StatusBar {
    //         anchors { left: parent.left; right: parent.right }
    //         color: Suru.backgroundColor
    //         // shown: text
    //         text: EvernoteConnection.error || NotesStore.error
    //         iconName: "sync-error"
    //         Suru.highlightType: Suru.NegativeHighlight
    //         iconColor: Suru.highlightColor
    //         showCancelButton: true

    //         onCancel: {
    //             NotesStore.clearError();
    //         }

    //         Timer {
    //             interval: 5000
    //             repeat: true
    //             running: NotesStore.error
    //             onTriggered: NotesStore.clearError();
    //         }
    //     }

    //     StatusBar {
    //         anchors { left: parent.left; right: parent.right }
    //         color: Suru.backgroundColor
    //         shown: importTransfer != null
    //         text: ((importTransfer != null) && (importTransfer.items.length === 1)) ?
    //                     i18n.tr("Select note to attach imported file") : i18n.tr("Select note to attach imported files")
    //         iconName: "document-save"
    //         showCancelButton: true

    //         onCancel: {
    //             importTransfer = null;
    //         }
    //     }
    // }

    Setup {
        id: setup
        applicationId: "notes.ubports_notes"
        providerId: useSandbox ? "notes.ubports_evernote-account-plugin-sandbox" : "notes.ubports_evernote-account-plugin"
    }

    Component {
        id: noAccountDialog
        Dialog {
            id: noAccount
            objectName: "noAccountDialog"
            title: i18n.tr("Sync with Evernote")
            text: i18n.tr("Notes can be stored on this device, or optionally synced with Evernote.") + " "
                          + i18n.tr("To sync with Evernote, you need an Evernote account.")

            Connections {
                target: accounts
                onCountChanged: {
                    if (accounts.count == 1) {
                        PopupUtils.close(noAccount)
                        doLogin();
                    }
                }
            }

            RowLayout {
                Button {
                    objectName: "openAccountButton"
                    text: i18n.tr("Not Now")
                    onClicked: {
                        PopupUtils.close(noAccount)
                        as.startAuthentication("@local", null);
                    }
                    Layout.fillWidth: true
                }
                Button {
                    objectName: "openAccountButton"
                    text: i18n.tr("Set Up…")
                    Suru.highlightType: Suru.PositiveHighlight
                    color: Suru.highlightColor
                    onClicked: setup.exec()
                    Layout.fillWidth: true
                }
            }
        }
    }

    Component {
        id: importQuestionComponent

        Dialog {
            id: importDialog
            title: importTransfer.items.length > 1 ?
                       i18n.tr("Importing %1 items").arg(importTransfer.items.length)
                     : i18n.tr("Importing 1 item")
            text: importTransfer.items.length > 1 ?
                      i18n.tr("Do you want to create a new note for those items or do you want to attach them to an existing note?")
                    : i18n.tr("Do you want to create a new note for this item or do you want to attach it to an existing note?")

            signal accepted(bool createNew);
            signal rejected();

            Button {
                text: i18n.tr("Create new note")
                onClicked: importDialog.accepted(true)
                Suru.highlightType: Suru.PositiveHighlight
                color: Suru.highlightColor
            }
            Button {
                text: i18n.tr("Attach to existing note")
                onClicked: importDialog.accepted(false);
            }
            Button {
                text: i18n.tr("Cancel import")
                onClicked: importDialog.rejected();
            }
        }
    }

    WebSocketServer {
        id: wsServer
        listen: true
        port: NoteHelper.port

        onErrorStringChanged: {
            print("Websocket server error: " + errorString)
        }
    }
}
